import os

from ij import *
import ij
import ij.IJ

def segmentNucleus(imp, lis, lpixs, config):
	results_path = config["RESULTS_PATH"]
	nucleus_snr = config["NUCLEUS_SNR"]
	title = imp.getTitle()+"_nucleus"
	imp_original = imp
	imp = imp.duplicate()
	imp.setTitle(title)
	IJ.run(imp, "Gaussian Blur 3D...", "x=2 y=2 z=10")
	if config["saveGraphicalOutput"]:
		IJ.save(imp, os.path.join(results_path, title+".tif")) 

	xcs = []
	ycs = []
	nis = []
	npixs = []
	
	for n in range(imp.getNSlices()):
		imp.setSliceWithoutUpdate(n+1)
		ip = imp.getProcessor()

		# grab the minimum values
		min_values = [9999]*ip.getWidth()*4
		pixels = ip.getPixels()
		local_max = 9999
		
		for i in range(ip.getPixelCount()):
			 v = pixels[i]
			 if v < local_max:
			 	idx = min_values.index(local_max)
			 	min_values[idx] = v
			 	local_max = max(min_values)

		# calculate and apply threshold
		bck = sum(min_values)/len(min_values)
		std = (sum([(bck-v)**2 for v in min_values])/float(len(min_values)))**0.5
		localThresh = bck+std*nucleus_snr				
		ip.min(localThresh)
		ip.add(-localThresh)

	# make image binary (0-1)  and fill holes
	IJ.run(imp, "Macro...", "code=v=(v>0) stack")
	IJ.run(imp, "Make Binary", "calculate black")
	IJ.run(imp, "Fill Holes", "stack")
	IJ.run(imp, "16-bit", "")
	IJ.run(imp, "Divide...", "value="+str(255./50)+" stack");
		
	for n in range(imp.getNSlices()):
		imp.setSliceWithoutUpdate(n+1)
		ip = imp.getProcessor()
		pixels = ip.getPixels()

		imp_original.setSliceWithoutUpdate(n+1)
		ip_original = imp_original.getProcessor()
		pixels_original = ip_original.getPixels()
		
		# find the center pixel
		xc = 0
		yc = 0
		npixels = 0.
		mean_intens = 0.
		for i in range(ip.getPixelCount()):
			
			# calculate the nucleus center of slice n
			v = pixels[i]
			v_original = pixels_original[i]
			if v > 0:
				x_idx = i%ip.getWidth()
				y_idx = int(i/ip.getWidth())
				xc += x_idx
				yc += y_idx
				npixels += 1.
				mean_intens += v_original
		# nucleus center
		xc /= npixels or 1
		yc /= npixels or 1
		xcs.append(xc+.5)
		ycs.append(yc+.5)

		# nucleus mean intensity
		if npixels>lpixs[n]:
			mean_intens = (mean_intens-(lis[n]*lpixs[n]))/(npixels-lpixs[n])
		else:
			mean_intens = lis[n]
		nis.append(mean_intens)
		npixs.append(npixels)

	# dump nucleus coordinates
	f = file(os.path.join(results_path, title+"_positions.txt"), 'w')
	for n in range(len(xcs)):
		f.write("%.5f\t%.5f\t%.5f\t%.5f\n" % (xcs[n], ycs[n], nis[n], npixs[n]))
	f.close()
	
	# dump nucleus mask
	title = title+"_mask"
	if config["saveGraphicalOutput"]:
		IJ.save(imp, os.path.join(results_path, title+".tif"))

	return xcs, ycs, imp, nis, npixs # End function segmentNucleus
