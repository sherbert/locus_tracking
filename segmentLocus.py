#################### Detect custom jython libs ######################
import sys, os; from ij import Prefs
_prefs = Prefs(); _path = _prefs.get("JythonExtra.path", "")
if _path == "" or not os.path.exists(_path):
        _path = IJ.getDirectory("Please select 'jython-libs'")[:-1]
_prefs.set("JythonExtra.path", _path); _prefs.savePreferences()
sys.path.append(_path)
#####################################################################

from ij import *
import ij.plugin.ImageCalculator as ImageCalculator


from subPixelLoc import *
from calculationSNR import calculationSNR


def segmentLocus(imp, config):
	title = imp.getTitle()+"_locus"
	imp_original = imp
	imp = imp.duplicate()
	imp.setTitle(title)

	imp_smooth = imp.duplicate()
	IJ.run(imp_smooth, "Smooth", "stack")

	w = imp.getWidth()
	h = imp.getHeight()

	# noise substraction
	config["MAIN_LOCK"].acquire()
	imp_sub = imp.duplicate()
	#IJ.run(imp, "Gaussian Blur 3D...", "x=0.5 y=0.5 z="+str(TIME_SMOOTH))
	IJ.run(imp, "Gaussian Blur...", "sigma=0.5 stack")
	IJ.run(imp_sub, "Gaussian Blur...", "sigma=10 stack")
	ic = ImageCalculator()
   	ic.run("Subtract stack", imp, imp_sub)
   	imp_sub.close()
   	config["MAIN_LOCK"].release()
   
	if config["saveGraphicalOutput"]:
		IJ.save(imp, os.path.join(config["RESULTS_PATH"], title+".tif"))
	#imp.show(); imp=imp.duplicate()

	xcs = [] # locus x position
	ycs = [] # locus y position
	zcs = [] # locus z position
	lis = [] # locus mean intensity
	lmis = [] # locus max intensity
	lpixs = [] # locus number of pix
	lcs = {} # locus confidence coefficient
	lcs['lxstd'] = []
	lcs['lystd'] = []
	lcs['maxSym'] = []
	lcs['xsym'] = []
	lcs['ysym'] = []
	lcs['gMaxVal'] = []
	lcs['gRefine'] = []
	lcs['gResidue'] = []
	lcs['SNR_max'] = []
	lcs['SNR_mean'] = []

	meanBck = []
	stdBck = []
	meanPSF = []
	stdPSF = []



		
	for n in range(imp.getNSlices()):
		imp.setSliceWithoutUpdate(n+1)
		imp_original.setSliceWithoutUpdate(n+1)
		imp_smooth.setSliceWithoutUpdate(n+1)
		
		ip = imp.getProcessor()
		ip_original = imp_original.getProcessor()
		ip_smooth = imp_smooth.getProcessor()

		pixels = ip.getPixels()
		pixels_original = ip_original.getPixels()

		# calculate and apply threshold - LOCUS INTENSITY BASED 

		############ !!!!!!!!!!!!!!!!!! box search test !!!!!!!!!!!!!!
		if config["boxSearchDev"] ==1:
			v_w, x_w, y_w = calculateIntegratedSum(imp, 20, 10)
			v_h, x_h, y_h = calculateIntegratedSum(imp, 10, 20)
			if v_w > v_h: 
				v, x, y = v_w, x_w, y_w
				print n, v, "y-axis wins..."
			else: 
				v, x, y = v_h, x_h, y_h
				print n, v, "x-axis wins..."
		############ !!!!!!!!!!!!!!!!!! box search test !!!!!!!!!!!!!!

		if len(xcs) == 0:
			max_value = max(pixels)
			i = pixels.index(max_value)
			x_idx = i%ip.getWidth()
			y_idx = int(i/ip.getWidth())
			
		else: # search for the max value in the vicinity of the previous max value
			max_value = 0
			xstart = int(round(xcs[-1]))-config["SEARCH_RADIUS"]
			ystart = int(round(ycs[-1]))-config["SEARCH_RADIUS"]
			xend = xstart+config["SEARCH_RADIUS"]*2+1
			yend = ystart+config["SEARCH_RADIUS"]*2+1
			if xstart < 0: xstart = 0
			if ystart < 0: ystart = 0
			if xend >= w: xend = w-1
			if yend >= h: yend = h-1
			for i in range(xstart, xend):
				for j in range(ystart, yend):
					v = ip.getf(i, j)
					if v > max_value:
						max_value = v
						x_idx = i
						y_idx = j
							 
		ip_mask = ip.duplicate()
		ip_mask.multiply(0)

		# Centroid calculation
		if config["SMOOTHED"]:
			xc, yc, maxValue, ip_mask, sum_v, npixels = centerMassDetection(x_idx, y_idx, w, h, ip_mask, ip_smooth, imp, config)
		else: 
			xc, yc, maxValue, ip_mask, sum_v, npixels = centerMassDetection(x_idx, y_idx, w, h, ip_mask, ip_original, imp, config)
		if config["CENTROID_METHOD"]=="gaussian fitting":
			if config["SMOOTHED"]:
				xc, yc, maxValue_gauss, refine, residue = gaussianPSFDetection(x_idx, y_idx, ip_smooth, config)
			else:
				xc, yc, maxValue_gauss, refine, residue = gaussianPSFDetection(x_idx, y_idx, ip_original, config)	
			lcs['gMaxVal'].append(maxValue_gauss)
			lcs['gRefine'].append(refine)
			lcs['gResidue'].append(residue)
		lmis.append(maxValue)
		xcs.append(xc+.5) # pourquoi +.5 ? because!!! because!!!
		ycs.append(yc+.5) # pourquoi +.5 ?

		# locus mean intensity
		mean_intens = float(sum_v)/float(npixels) or 1
		lis.append(mean_intens)
		lpixs.append(npixels)

		# locus confidence test
		conf = testConfidence(ip_original,xc,yc,config)
		SNR_max, SNR_mean, mean_Bck, std_Bck, mean_PSF, std_PSF = calculationSNR(ip_original, maxValue, mean_intens, xc, yc, config["RESULTS_PATH"], config["PSF_rad"])
		lcs['lxstd'].append(conf['lxstd'])
		lcs['lystd'].append(conf['lxstd'])
		lcs['maxSym'].append(conf['maxSym'])
		lcs['xsym'].append(conf['xsym'])
		lcs['ysym'].append(conf['ysym'])
		lcs['ysym'].append(conf['ysym'])
		lcs['SNR_max'].append(SNR_max)
		lcs['SNR_mean'].append(SNR_mean)
		meanBck.append(mean_Bck)
		stdBck.append(std_Bck)
		meanPSF.append(mean_PSF)
		stdPSF.append(std_PSF)

		if config["DO3D"]:
			zc = analyseAstigmatism(ip, ip_original, xc, yc) ## might recquire changes because previously based on ip_mask that is not produced anymore by the gaussian fitting
			zcs.append(zc)
		else:
			zcs.append(0)
	
	#imp.show()
			
	# dump locus mask
	if config["saveGraphicalOutput"]:
		IJ.save(imp, os.path.join(config["RESULTS_PATH"], title+"_mask.tif"))
	
	# dump locus coordinates
	f = file(os.path.join(config["RESULTS_PATH"], title+"_positions.txt"), 'w')
	for n in range(len(xcs)):
		#print zcs[n]
		f.write("%.5f\t%.5f\t%.5f\t%.5f\t%.5f\n" % (xcs[n], ycs[n], zcs[n], lis[n], lpixs[n]))
	f.close()

	# dump SNR infos
	f = file(os.path.join(config["RESULTS_PATH"], title+"_SNR.txt"), 'w')
	f.write("SNR_max\tSNR_mean\tmeanBck\tstdBck\tmeanPSF\tstdPSF\tmaxInt\n")
	for n in range(len(xcs)):
		f.write("%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\n" % (lcs['SNR_max'][n], lcs['SNR_mean'][n], meanBck[n], stdBck[n], meanPSF[n], stdPSF[n],lmis[n]))
	f.close()


	return xcs, ycs, zcs, imp, lis, lmis, lpixs, lcs

def testConfidence(ip_original, xc, yc, config): # many tests before seeing which one's the best
	w = ip_original.getWidth()
	h = ip_original.getHeight()
	
	sxdev = 0
        sydev = 0
        xlstd = 0
        xrstd = 0
        ylstd = 0
        yrstd = 0
        xlsum = 0
        xrsum = 0
        ylsum = 0
        yrsum = 0
        xstart = int(xc-config["PSF_rad"])
	ystart = int(yc-config["PSF_rad"])
	xend = int(xc+config["PSF_rad"]+1)
	yend = int(yc+config["PSF_rad"]+1)

	if xstart < 0 or ystart < 0 or xend >= w or yend >= h:
		confidence = {}
		confidence['lxstd']=999
		confidence['lystd']=999
		confidence['maxSym']=999
		confidence['xsym']=999
		confidence['ysym']=999
		return confidence

	for i in range(xstart, xend):
		for j in range(ystart, yend):
			#print i,j
			s=ip_original.get(i, j)

			sxdev = (i-xc)*s
			sydev = (j-yc)*s
			if sxdev <0: # to the left of the center
				xlstd += -sxdev
				xlsum += s
			else:
				xrstd += sxdev
				xrsum += s
			if sydev <0: 
				ylstd += -sydev
				ylsum += s
			else:
				yrstd += sydev
				yrsum += s
	xlstd/=xlsum or 1
	xrstd/=xrsum or 1
	ylstd/=ylsum or 1
	yrstd/=yrsum or 1
	lxstd = (xlstd+xrstd)/2
	lystd = (ylstd+yrstd)/2

	xsym = 1-abs((xlstd-xrstd)/(xlstd+xrstd));
        ysym = 1-abs((ylstd-yrstd)/(ylstd+yrstd));
        sym = []
        sym.append(xsym)
        sym.append(ysym)
        maxSym = max(sym)
                
	#print lxstd, lystd, maxSym, xsym, ysym
	confidence = {}
	confidence['lxstd']=lxstd
	confidence['lystd']=lystd
	confidence['maxSym']=maxSym
	confidence['xsym']=xsym
	confidence['ysym']=ysym

	#print confidence
	
	return confidence
