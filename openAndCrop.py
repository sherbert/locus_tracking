#################### Detect custom jython libs ######################
import sys, os; from ij import Prefs
_prefs = Prefs(); _path = _prefs.get("JythonExtra.path", "")
if _path == "" or not os.path.exists(_path):
        _path = IJ.getDirectory("Please select 'jython-libs'")[:-1]
_prefs.set("JythonExtra.path", _path); _prefs.savePreferences()
sys.path.append(_path)
#####################################################################

from ij import *
import glob
import os
import ij.plugin.frame.RoiManager as RoiManager
import ij.plugin.frame.PlugInFrame
import ij.gui.Roi as Roi
import ij.ImageStack as ImageStack

def openAndCrop(label, config):

	# loading ROIs positions
	roim = RoiManager(1)
	if roim.getCount()>0:
		roim.runCommand("Select All")
		roim.runCommand("Delete")

	roisList = []

	if os.path.exists(os.path.join(config["ROI_FILE"],"RoiSet.zip")): 
		print "using ROI file built by FIJI"
		roim.runCommand("Open", os.path.join(config["ROI_FILE"], "RoiSet.zip"))
		for roi in roim.getRoisAsArray():
			roisList.append(roi)
	elif os.path.exists(os.path.join(config["ROI_FILE"],"total_ROI.txt")):
		print "using ROI file built by matlab"
		#config["ROI_FILE"] = os.path.join(config["ROI_FILE"],"total_ROI")
		roi_text = open(os.path.join(config["ROI_FILE"],"total_ROI.txt")).read()
		for line in roi_text.split("\n")[:-1]:
			values = map(float, line.split()) # convert strings to floats
			roisList.append(Roi(values[0], values[2], values[1]-values[0], values[3]-values[2]))
	else : 
		print "no ROI file found"
		
	roisStack = []
	for roi in roisList:
		#print roi.getBounds().width
		roisStack.append(ImageStack(roi.getBounds().width,roi.getBounds().height))
		
	listFile = glob.glob(config["BASE_IMAGE"]+"/*"+label)
	listFile.sort()
	
	for image in listFile[config["BASE_IMAGE_START"]:config["BASE_IMAGE_END"]]:
		imp = IJ.openImage(image)
		ip = imp.getProcessor()
		roiNumber = 0
		for roi in roisList: #(0,len(roisList)-1):
			ip.setRoi(roi)
			ipRoi = ip.crop()
			roisStack[roiNumber].addSlice(ipRoi)
			roiNumber = roiNumber+1
			
	return roisStack