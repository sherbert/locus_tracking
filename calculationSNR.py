import os

from ij import *
import ij
import ij.IJ

def calculationSNR(ip, maxSignal, meanSignal, xCenter, yCenter, results_path, PSF_rad):
	
	bckPix = {}
	bckPix["xPos"] = []
	bckPix["yPos"] = []
	bckPix["value"] = []

	PSFPix = {}
	PSFPix["xPos"] = []
	PSFPix["yPos"] = []
	PSFPix["value"] = []

#	xPSFstart = int(xCenter-PSF_rad)
#	yPSFstart = int(yCenter-PSF_rad)
#	xPSFend = int(xCenter+PSF_rad+1)
#	yPSFend = int(yCenter+PSF_rad+1)
	
	xSNRstart = int(xCenter-PSF_rad*2)
	ySNRstart = int(yCenter-PSF_rad*2)
	xSNRend = int(xCenter+PSF_rad*2+1)
	ySNRend = int(yCenter+PSF_rad*2+1)

	for i in range(xSNRstart, xSNRend):
		for j in range(ySNRstart, ySNRend):
			pixVal = ip.getPixel(i,j)
			if pixVal != 0: # if = 0, coordinates are out of range
				if ((xCenter-i)**2+(yCenter-j)**2)**0.5 <= PSF_rad:
					PSFPix["xPos"].append(i)
					PSFPix["yPos"].append(j)
					PSFPix["value"].append(pixVal)				
				
				elif ((xCenter-i)**2+(yCenter-j)**2)**0.5 <= PSF_rad*2:
					bckPix["xPos"].append(i)
					bckPix["yPos"].append(j)
					bckPix["value"].append(pixVal)				
			else:
				if ((xCenter-i)**2+(yCenter-j)**2)**0.5 <= PSF_rad:
					return 1,1,1,1,1,1 # End function calculationSNR in case PSF is out of bounderies
														
	meanBck, stdBck = meanAndStd(bckPix["value"])
	meanPSF, stdPSF = meanAndStd(PSFPix["value"])

	SNR_max = (maxSignal-meanBck)/stdBck
	SNR_mean = (meanSignal-meanBck)/stdBck

	return SNR_max, SNR_mean, meanBck, stdBck, meanPSF, stdPSF # End function calculationSNR


def meanAndStd(l):
	mean = float(sum(l))/len(l)
	std = sum([(v-mean)**2/len(l) for v in l])**0.5
	return mean, std



	