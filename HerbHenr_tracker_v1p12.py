# Herbert and Henriques productions present... another MDF awesome macro...


#################### Detect custom jython libs ######################
import sys, os
_prefs = Prefs(); _path = _prefs.get("JythonExtra.path", "")
if _path == "" or not os.path.exists(_path):
        _path = IJ.getDirectory("Please select 'jython-libs'")[:-1]
_prefs.set("JythonExtra.path", _path); _prefs.savePreferences()
sys.path.append(_path)
#####################################################################


import os, shutil, threading, time
import ij.plugin.frame.RoiManager
import ij.gui.Roi
import ij.plugin.ImageCalculator
import ij.io.DirectoryChooser
import ij.gui.NonBlockingGenericDialog
import ij.Prefs
import math
import ij.gui.Plot

# my imports
from segmentNucleus import segmentNucleus
from segmentLocus import segmentLocus


PREFS = Prefs()

################## FIXED PARAMETERS ##################
CONFIG = {}

# PROGRAM VERSION
CONFIG["VERSION"] = "v1p12"

# COMPUTER CAPACITIES
CONFIG["NCPUS"] = PREFS.get("HerbHenr_tracker.ncpus", 8)
CONFIG["ENABLE_THREADING"] = bool(PREFS.get("HerbHenr_tracker.threading", 0))

# TIF FILES LOADING PARAMETERS
CONFIG["BASE_IMAGE_START"] = 0
CONFIG["BASE_IMAGE_NFRAMES"] = 999
CONFIG["NBRE_CHANNELS"] = PREFS.get("HerbHenr_tracker.nbre_channels", 1)

# NUCLEUS SEGMENTATION PARAMETERS
CONFIG["NUCLEUS_SNR"] = 10  # threshold between bck and nucleoplasm

# LOCUS SEGMENTATION PARAMETERS
CONFIG["GRAD_TOLERANCE"] = 1.2
CONFIG["TIME_SMOOTH"] = 0
CONFIG["LOCAL_THRESH"] = 0.75
CONFIG["SEARCH_RADIUS"] = 4
CONFIG["DO3D"] = 0
CONFIG["boxSearchDev"] = 0

# CENTROID CALCULATION PARAMETERS
CONFIG["SMOOTHED"] = 0
CONFIG["MASS_CENTER"] = 1
CONFIG["CENTROID_METHOD"] = "gaussian fitting"

# COLOR MERGE
CONFIG["MAGN"] = 5
CONFIG["Seuil_Iratio"] = 1.5
CONFIG["Seuil_SNR_max"] = 5
CONFIG["Seuil_SNR_mean"] = 1.5

# SAVE ANALYSED IMAGES
CONFIG["saveGraphicalOutput"] = bool(PREFS.get("HerbHenr_tracker.saveGraphicalOutput", 0))

# NEED FOR THREADING TO WORK
CONFIG["MAIN_LOCK"] = threading.Lock()

# NEED FOR DYNAMIC ANALYSIS
CONFIG["timeStep"] = 100 # msec
CONFIG["pixelSize"] =  108 # nm
#PSF_rad = math.floor(300/pixelSize)
CONFIG["PSF_rad"] = 3

################## FIXED PARAMETERS ##################

od = OpenDialog("Select base path...", ".")
f = od.getFileName()
if f is None: 
	print "exiting..."
print f

dc = DirectoryChooser("Select path to analyse...")
LOCAL_PATH = dc.getDirectory()
if LOCAL_PATH is None: raise Exception("No folder chosen...")
CONFIG["LOCAL_PATH"] = LOCAL_PATH[:-1] # delete "/" at the end of the line

CONFIG["BASE_PATH"], CONFIG["LOCAL_PATH"] = os.path.split(CONFIG["LOCAL_PATH"])
print "Processing folder '%s' under '%s'..." % (CONFIG["LOCAL_PATH"], CONFIG["BASE_PATH"])

CONFIG["BASE_IMAGE"] = os.path.join(CONFIG["BASE_PATH"], CONFIG["LOCAL_PATH"])
CONFIG["RESULTS_PATH"] = os.path.join(CONFIG["BASE_PATH"], CONFIG["LOCAL_PATH"]+"_results", "results")
CONFIG["ROIS_PATH"] = os.path.join(CONFIG["BASE_PATH"], CONFIG["LOCAL_PATH"]+"_results", "rois")


gd = NonBlockingGenericDialog("Analysis preferences...")
gd.addMessage("HerbHen tracker V"+CONFIG["VERSION"])

gd.addMessage("Computer capacities") # Computer capacities
gd.addNumericField("Number of CPU : ",CONFIG["NCPUS"],0)
gd.addCheckbox("Enable threading", CONFIG["ENABLE_THREADING"])

gd.addMessage("tif files loading parameters") # tif files loading parameters
gd.addNumericField("First frame to analyse :", CONFIG["BASE_IMAGE_START"], 0)
gd.addNumericField("Last frame to analyse :", CONFIG["BASE_IMAGE_NFRAMES"], 0)
gd.addNumericField("Number of channels : ",CONFIG["NBRE_CHANNELS"],0) # Number of channels of fluorescence

gd.addMessage("Nucleus segmentation parameters") # Nucleus segmentation parameters
gd.addNumericField("Nucleoplasm vs Noise SNR :", CONFIG["NUCLEUS_SNR"], 0)

gd.addMessage("Locus segmentation parameters")# Locus segmentation parameters
gd.addNumericField("Search radius (pixels) :", CONFIG["SEARCH_RADIUS"], 0)
#gd.addCheckbox("3D Analysis", CONFIG["DO3D"])
gd.addCheckbox("smooth image", CONFIG["SMOOTHED"])
gd.addChoice("Centroid calculation",["center of mass","gaussian fitting"],CONFIG["CENTROID_METHOD"])

gd.addMessage("Experimental settings")
gd.addNumericField("Time step (msec) : ", CONFIG["timeStep"],0)
gd.addNumericField("Pixel size (nm) : ", CONFIG["pixelSize"],0)

gd.addMessage("Color merge witness tresholds settings")
gd.addNumericField("Threshold for Iratio : ", CONFIG["Seuil_Iratio"],1)
gd.addNumericField("Threshold for SNR max : ", CONFIG["Seuil_SNR_max"],1)
gd.addNumericField("Threshold for SNR mean : ", CONFIG["Seuil_SNR_mean"],1)

gd.addCheckbox("Save analysed images in separate files", CONFIG["saveGraphicalOutput"])

gd.showDialog()
if (gd.wasCanceled()): raise Exception("Dialog canceled...")

# Computer capacities
CONFIG["NCPUS"] = int(gd.getNextNumber())
PREFS.set("HerbHenr_tracker.ncpus", CONFIG["NCPUS"])
CONFIG["ENABLE_THREADING"] = gd.getNextBoolean()
PREFS.set("HerbHenr_tracker.threading", CONFIG["ENABLE_THREADING"])

# tif files loading parameters
CONFIG["BASE_IMAGE_START"] = int(gd.getNextNumber())
CONFIG["BASE_IMAGE_NFRAMES"] = int(gd.getNextNumber())
CONFIG["NBRE_CHANNELS"] = int(gd.getNextNumber())
PREFS.set("HerbHenr_tracker.nbre_channels", CONFIG["NBRE_CHANNELS"])

# Nucleus segmentation parameters
CONFIG["NUCLEUS_SNR"] = int(gd.getNextNumber())

# Locus segmentation parameters
CONFIG["SEARCH_RADIUS"] = int(gd.getNextNumber())

#CONFIG["DO3D"] = gd.getNextBoolean()
CONFIG["SMOOTHED"] = gd.getNextBoolean()
CONFIG["CENTROID_METHOD"] = gd.getNextChoice()

# Experimental settings
CONFIG["timeStep"] = int(gd.getNextNumber()) # msec
CONFIG["pixelSize"] = int(gd.getNextNumber()) # nm

# Color merge witness tresholds settings
CONFIG["Seuil_Iratio"] = int(gd.getNextNumber())
CONFIG["Seuil_SNR_max"] = int(gd.getNextNumber())
CONFIG["Seuil_SNR_mean"] = int(gd.getNextNumber())

#Save analysed images
CONFIG["saveGraphicalOutput"] = gd.getNextBoolean()
PREFS.set("HerbHenr_tracker.saveGraphicalOutput", CONFIG["saveGraphicalOutput"])

PREFS.savePreferences()



################## FUNCTIONS ##################

def calculateIntegratedSum(imp, roiWidth, roiHeight):
	ip = imp.getProcessor()

	w = ip.getWidth()
	h = ip.getHeight()

	x0 = 0
	y0 = 0
	v0 = 0
	
	for i in range(w-roiWidth):
		for j in range(h-roiHeight):
			v = 0
			for roi_i in range(roiWidth):
				for roi_j in range(roiHeight):
					v += ip.getPixel(i+roi_i, j+roi_j)
			if v > v0:
				v0 = v
				x0 = i
				y0 = j

	return v0, x0, y0


def analyseAstigmatism(ip, ip_original, xc, yc):
	if os.path.exists("/media/data2/Datas_microscopie/astigmatism/60x/beads_zstack_20120803_62512/version1/Astigmatismcalibration.csv"):
		# calibration = open("/media/data2/Datas_microscopie/astigmatism/beads_zstack_20120803_62512 PM/Astigmatism calibration.xls").read()
		# calibration = xlrd.open_workbook("/media/data2/Datas_microscopie/astigmatism/beads_zstack_20120803_62512 PM/Astigmatism calibration.xls")
		calibration = open("/media/data2/Datas_microscopie/astigmatism/60x/beads_zstack_20120803_62512/version1/Astigmatismcalibration.csv","r")
		calibration.readline()
		full = calibration.readlines()

		z_calib = []
		wmh_calib = []
		for rownum in range(len(full)):
			ligne = full[rownum].split()
			z_calib.append(float(ligne[1]))
			wmh_calib.append(float(ligne[3]))
		#print z_calib, wmh_calib

	
	
	else: 
		print "There is no 'Astigmatism calibration.xls' file at the given adress\nSwitching for 2D analysis"
		return
		
		
        window_radius = 5
        percentil_thresh = 0.2

	w = ip.getWidth()
	h = ip.getHeight()
	local_thresh = ip.get(int(round(xc)), int(round(yc)))
	
	sxdev = 0
        sydev = 0
        xlstd = 0
        xrstd = 0
        ylstd = 0
        yrstd = 0
        xlsum = 0
        xrsum = 0
        ylsum = 0
        yrsum = 0
        xstart = int(xc-window_radius)
	ystart = int(yc-window_radius)
	xend = int(xc+window_radius+1)
	yend = int(yc+window_radius+1)
	#print xstart, ystart
	#print xend, yend
	#print w, h
	if xstart < 0 or ystart < 0 or xend >= w or yend >= h:
		zc = float('nan')
		return zc
		#raise "Bound boinc boinc, your fucked"

	for i in range(xstart, xend):
		for j in range(ystart, yend):
			s=ip_original.get(i, j)
			if s < local_thresh: continue

			sxdev = (i-xc)*s
			sydev = (j-yc)*s
			if sxdev <0: # to the left of the center
				xlstd += -sxdev
				xlsum += s
			else:
				xrstd += sxdev
				xrsum += s
			if sydev <0: 
				ylstd += -sydev
				ylsum += s
			else:
				yrstd += sydev
				yrsum += s
	xlstd/=xlsum or 1
	xrstd/=xrsum or 1
	ylstd/=ylsum or 1
	yrstd/=yrsum or 1

	wmh = ((xlstd+xrstd)-(ylstd+yrstd))*1.177 # width minus height

	print wmh
	#print wmh_calib[0]
	#print wmh_calib[len(wmh_calib)-1]

	if wmh>wmh_calib[0] or wmh<wmh_calib[len(wmh_calib)-1]:
		zc = float('nan')
		return zc
	else:
		for row in range(len(wmh_calib)):
			if wmh>wmh_calib[row]:
				x2 = wmh_calib[row] # inf_bound_wmh
				x1 = wmh_calib[row-1] # sup_bound_wmh
				z2 = z_calib[row] # inf_bound_z
				z1 = z_calib[row-1] # sup_bound_z

				z = z1+((z2-z1)/(x2-x1))*(wmh-x1)

				return z
	# print xlstd, xrstd, ylstd, yrstd
			
def plotCenters(imp, nxcs, nycs, lxcs, lycs,lcs,lmis,nis): # plot centers of the locus and the nucleolus 					
	imp = imp.duplicate()

	w = imp.getWidth()
	h = imp.getHeight()
	nslices = imp.getNSlices()

	IJ.run(imp, "Multiply...", "value=0 stack")
	IJ.run(imp, "Size...", "width="+str(w*CONFIG["MAGN"])+" height="+str(h*CONFIG["MAGN"])+" depth="+str(nslices)+" interpolation=None");

	# plot cross
	for n in range(nslices):
		imp.setSliceWithoutUpdate(n+1)
		ip = imp.getProcessor()

		# nucleus centers
		x = int(round(nxcs[n]*CONFIG["MAGN"]))
		y = int(round(nycs[n]*CONFIG["MAGN"]))
		if x > 0 and y > 0 and x < w*CONFIG["MAGN"]-1  and y < h*CONFIG["MAGN"]-1:
			ip.setf(x, y, 10000)
			ip.setf(x-1, y, 10000)
			ip.setf(x+1, y, 10000)
			ip.setf(x, y-1, 10000)
			ip.setf(x, y+1, 10000)

		# locus centers
		x = int(round(lxcs[n]*CONFIG["MAGN"]))
		y = int(round(lycs[n]*CONFIG["MAGN"]))
		if x > 1 and y > 1 and x < w*CONFIG["MAGN"]-2  and y < h*CONFIG["MAGN"]-2:
			ip.setf(x, y, 10000)
			ip.setf(x-2, y, 10000)
			ip.setf(x+2, y, 10000)
			ip.setf(x, y-2, 10000)
			ip.setf(x, y+2, 10000)

			#ip.setf(x-1, y-1, 1000)
			#ip.setf(x-1, y+1, 1000)
			#ip.setf(x+1, y-1, 1000)
			#ip.setf(x+1, y+1, 1000)

		# confidence coef
		
		coef = lmis[n]/float(nis[n]) 
		if coef>CONFIG["Seuil_Iratio"]: Coefbin = 10000 # down left corner
		else: Coefbin = 0
		posy = 0		
		for posx in range(0,CONFIG["MAGN"]):
			for posy in range(h*CONFIG["MAGN"]-CONFIG["MAGN"],h*CONFIG["MAGN"]):
				ip.setf(posx,posy, Coefbin)

		
		if lcs['SNR_max'][n]>CONFIG["Seuil_SNR_max"]: maxSNRtresh = 10000 # up left corner
		else: maxSNRtresh = 0
		posy = 0		
		for posx in range(0,CONFIG["MAGN"]):
			for posy in range(0,CONFIG["MAGN"]):
				ip.setf(posx,posy, maxSNRtresh)

		if lcs['SNR_mean'][n]>CONFIG["Seuil_SNR_mean"]: meanSNRtresh = 10000 # up right corner
		else: meanSNRtresh = 0
		posy = 0		
		for posx in range(w*CONFIG["MAGN"]-CONFIG["MAGN"],w*CONFIG["MAGN"]):
			for posy in range(0,CONFIG["MAGN"]):
				ip.setf(posx,posy, meanSNRtresh)
	
	w = imp.getWidth()
	h = imp.getHeight()

	
	return imp

def fitNucleoplasm(imp_roi, nxcs, nycs):
	title = imp_roi.getTitle()
	imp_driftCorrected = imp_roi.duplicate()

	for n in range(imp_driftCorrected.getNSlices()):
		dx = nxcs[0] - nxcs[n]
		dy = nycs[0] - nycs[n]
	
		imp_driftCorrected.setSlice(n)
		IJ.run(imp_driftCorrected, "Translate...", "x=%.5f y=%.5f interpolation=Bicubic slice" % (dx, dy));

	if CONFIG["saveGraphicalOutput"]:
		IJ.save(imp_driftCorrected, os.path.join(CONFIG["RESULTS_PATH"], title+"_driftCorrected.tif"))

	
	#imp_driftCorrected.show()
	IJ.run(imp_driftCorrected, "Z Project...", "start=1 stop=10 projection=[Max Intensity]")
	imp_proj = IJ.getImage()

	ip_proj = imp_proj.getProcessor()
	ip_proj.setAutoThreshold("Mean", True, True)
	ts = ThresholdToSelection()
	roi = ts.convert(ip_proj)
	imp_proj.setRoi(roi, True)
	IJ.run(imp_proj, "Fit Ellipse", "")
	IJ.run(imp_proj, "Set Measurements...", "  centroid fit redirect=None decimal=3")
	IJ.run(imp_proj, "Measure", "")
	results = Analyzer.getResultsTable() 
	x = results.getValue("X", results.getCounter()-1) 
	y = results.getValue("Y", results.getCounter()-1) 
	maj = results.getValue("Major", results.getCounter()-1)
	min = results.getValue("Minor", results.getCounter()-1)
	angle = results.getValue("Angle", results.getCounter()-1)
	roi = imp_proj.getRoi()

	if CONFIG["saveGraphicalOutput"]:
		IJ.save(imp_proj, os.path.join(CONFIG["RESULTS_PATH"], title+"_ellipse.tif"))
	imp_proj.changes = False
	imp_proj.close()

	# STOP HERE FOR RADIUS ONLY
	IJ.run("Clear Results");
	return x,y,maj,min,angle 


		
	imp_driftCorrected.setRoi(roi)
	IJ.run(imp_driftCorrected, "Rotate...", "angle=%.3f" % angle)
	roi0d =  imp_driftCorrected.getRoi()
	
	IJ.run(imp_driftCorrected, "Gaussian Blur 3D...", "x=0.5 y=0.5 z="+str(CONFIG["TIME_SMOOTH"]))
	
	# go find the angle per frame
	for n in range(imp_driftCorrected.getNSlices()):
		imp_driftCorrected.setSlice(n+1)

		#imp_driftCorrected.setRoi(roi0d, True)

		ip = imp_driftCorrected.getProcessor()
		imp_temp = ImagePlus("temp", ip)
		imp_temp.show()
		#imp_temp.setRoi(roi0d, True)
		IJ.run(imp_temp, "Size...", "width=%d height=%d interpolation=Bicubic" % (ip.getWidth()*10, ip.getHeight()*10))
		IJ.run(imp_temp, "Specify...", "width="+str(maj*10)+" height="+str(min*10)+" x="+str(x*10)+" y="+str(y*10)+" oval centered")
		roi_temp = imp_temp.getRoi()
		ip_temp = imp_temp.getProcessor()

		#ip.setRoi(roi0d)
		#ip.setInterpolationMethod(ImageProcessor.BICUBIC) 
		#ip.scale(ip.getWidth()*10, ip.getHeight()*10)
		#imp_temp = ImagePlus("temp", ip)
		#imp_temp.setRoi(ip.getRoi(), True)

		max_v = 0
		max_a = 0
		for ang in range(180):
			IJ.run(imp_temp, "Rotate...", "angle=%.5f" % 1)
			stats = ip_temp.getStatistics()
			print ang, stats.mean, stats.pixelCount
			if stats.mean > max_v:
				max_v = stats.mean
				max_a = ang 
		print max_v, max_a
		wakawaka

	wakawaka

def showLocusHist(lxcs, lycs):
	deltaX = []
	deltaY = []
	deltaEucl = []
	time = []

	IJ.newImage("deltaEucl", "32-bit black", len(lxcs)-1, 1, 1) 
	ipDeltaEucl = IJ.getImage().getProcessor()

	IJ.newImage("deltaAxis", "32-bit black", len(lxcs)-1, 2, 1) 
	ipDeltaAxis = IJ.getImage().getProcessor()

	for n in range(len(lxcs)-1):
		deltaX.append(lxcs[n+1]-lxcs[n])
		deltaY.append(lycs[n+1]-lycs[n])
		d = (deltaX[n]**2+deltaY[n]**2)**0.5
		deltaEucl.append(d)
		time.append(n)
		ipDeltaEucl.setf(n, 0, d)
		ipDeltaAxis.setf(n, 0, deltaX[n])
		ipDeltaAxis.setf(n, 1, deltaY[n])
		
	#plot = Plot("X-Position", "time", "value", time+[n+1], lxcs)
	#plot = Plot("Deltas", "time", "value", time, deltaEucl)
	#plot.addPoints(time, deltaX, Plot.LINE)
	#plot.addPoints(time, deltaY, Plot.LINE)
	#plot.show()
	#wakawaka  

def analyseRoi(imp_roi, title, CONFIG):
	imp_roi.setTitle(title)
	IJ.save(imp_roi, os.path.join(CONFIG["ROIS_PATH"], title+".tif")) 

	if 1: # put at 0 to only save ROIs

		print "Analysing Locus"
		lxcs, lycs, lzcs, limp, lis, lmis, lpixs, lcs = segmentLocus(imp_roi, CONFIG)
		print "Analysing nucleus"
		nxcs, nycs, nimp, nis, npixs = segmentNucleus(imp_roi, lis, lpixs, CONFIG)

		if CONFIG["saveGraphicalOutput"]:
			print "Plotting centers"
			cimp =  plotCenters(imp_roi, nxcs, nycs, lxcs, lycs,lcs,lmis,nis)
			#showLocusHist(lxcs, lycs)

			nell_x, nell_y, nell_maj, nell_min, nell_angle = fitNucleoplasm(imp_roi, nxcs, nycs)
	
			w = imp_roi.getWidth()
			h = imp_roi.getHeight()
			nslices = imp_roi.getNSlices()
 
			CONFIG["MAIN_LOCK"].acquire()

			cimp.show()
			nimp.show()
			limp.show()
			imp_roi.show()
			
			IJ.run(imp_roi, "Size...", "width="+str(w*CONFIG["MAGN"])+" height="+str(h*CONFIG["MAGN"])+" depth="+str(nslices)+" interpolation=None");
			IJ.run(limp, "Size...", "width="+str(w*CONFIG["MAGN"])+" height="+str(h*CONFIG["MAGN"])+" depth="+str(nslices)+" interpolation=None");
			IJ.run(nimp, "Size...", "width="+str(w*CONFIG["MAGN"])+" height="+str(h*CONFIG["MAGN"])+" depth="+str(nslices)+" interpolation=None");
		
			IJ.run("Merge Channels...", "c1="+imp_roi.getTitle()+" c2="+limp.getTitle()+" c3="+nimp.getTitle()+" c4="+cimp.getTitle()+" create")
			imp_rgb = IJ.getImage()
			imp_rgb.hide()
			IJ.save(imp_rgb, os.path.join(CONFIG["RESULTS_PATH"], title+"_colorMerge.tif"))
			imp_rgb.close()
			CONFIG["MAIN_LOCK"].release()

	# dump info
	# dump datas
	f = file(os.path.join(CONFIG["RESULTS_PATH"], title+"_data.txt"), 'w') # results of the analysis
	f.write("Locus X (px)\tLocus Y (px)\tLocus Z (px)\tNucleus X (px)\tNucleus Y (px)\tIntensity ratios\tLocus MaxInt\tSNR_max\tSNR_mean\n")
	for n in range(len(lxcs)):
		#coef = 1.-(float(nis[n])/float(lis[n])) # old version doesn't work
		coef = lmis[n]/float(nis[n])
		#IJ.log("image = %d   lmis[n]=%d    float(nis[n])=%d" % (n, lmis[n], float(nis[n])))
		f.write("%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t%.3f\t%.3f\n" % (lxcs[n], lycs[n], lzcs[n], nxcs[n], nycs[n], coef, lmis[n],lcs['SNR_max'][n],lcs['SNR_mean'][n]))
	f.close()

	# dump several suppl parameters
	suppl = file(os.path.join(CONFIG["RESULTS_PATH"], title+"_suppl.txt"), 'w') # confidence of spot detection ROI + area locus and nucleoplasm
	if CONFIG["CENTROID_METHOD"]=="center of mass":
		# print "saving center of mass style"
		suppl.write("SNR_max\tSNR_mean\txstd\tystd\tmaxSym\txsym\tysym\tIratio\tlocus area(px)\tnucleus area(px)\tCM maxValue\n")
		for n in range(len(lxcs)):
			coef = lmis[n]/float(nis[n])
			suppl.write("%.3f\t%.3f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\n" % \
				(lcs['SNR_max'][n],lcs['SNR_mean'][n],lcs['lxstd'][n],lcs['lystd'][n],lcs['maxSym'][n],lcs['xsym'][n],lcs['ysym'][n],coef,lpixs[n],npixs[n],lmis[n]))
		suppl.close()
	elif CONFIG["CENTROID_METHOD"]=="gaussian fitting":
		# print "saving gaussian fitting style"
		suppl.write("SNR_max\tSNR_mean\txstd\tystd\tmaxSym\txsym\tysym\tIratio\tlocus area(px)\tnucleus area(px)\tCM maxValue\tGF maxValue\tgaussian refine\tgaussian residue\n")
		for n in range(len(lxcs)):
			coef = lmis[n]/float(nis[n])
			suppl.write("%.3f\t%.3f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\n" % \
				(lcs['SNR_max'][n],lcs['SNR_mean'][n],lcs['lxstd'][n],lcs['lystd'][n],lcs['maxSym'][n],lcs['xsym'][n],lcs['ysym'][n],coef, \
				lpixs[n],npixs[n],lmis[n],lcs['gMaxVal'][n],lcs['gRefine'][n],lcs['gResidue'][n]))
		suppl.close()
	else: print "Warning Conf file not saved => Centroid method not detected"

#	# dump ellipse values
#	ellipse = file(os.path.join(CONFIG["RESULTS_PATH"], title+"_ellipse.txt"), 'w') # fitted ellipse of the nucleoplasm
#	ellipse.write("xCent\tyCent\tmajAxis\tminAxis\tAngle\n")
#	ellipse.write("%.5f\t%.5f\t%.5f\t%.5f\t%.5f\n" % (nell_x, nell_y, nell_maj, nell_min, nell_angle))
#	ellipse.close()


def main():
	IJ.run("Close All")
	baseResultsPath = CONFIG["RESULTS_PATH"]
	
	for channel in range(CONFIG["NBRE_CHANNELS"]):

		IJ.log("loading channel : "+str(channel))
		
		# adapt label of the image to deal with different channels
		if (CONFIG["NBRE_CHANNELS"])>1:
			label = "w000"+str(channel)+".tif"
			CONFIG["RESULTS_PATH"] = baseResultsPath+"_channel"+str(channel)
		else:
			label = ".tif"
			
		# load up image
		if os.path.isdir(CONFIG["BASE_IMAGE"]):
			IJ.run("Image Sequence...", "open=["+CONFIG["BASE_IMAGE"]+"] number="+str(CONFIG["BASE_IMAGE_NFRAMES"])+" starting="+str(CONFIG["BASE_IMAGE_START"])+" increment=1 scale=100 file="+label+" or=[] sort")
			imp = IJ.getImage() #grab reference to the image
			CONFIG["ROI_FILE"] = os.path.join(CONFIG["BASE_PATH"], CONFIG["LOCAL_PATH"])
		else:
			imp = IJ.openImage(CONFIG["BASE_IMAGE"]);
			CONFIG["ROI_FILE"] = CONFIG["BASE_PATH"]
			
		try:
			imp.hide()
		except AttributeError:
			print "Error : No .tif file found at the given adress. Check the adress"
			return
	
		roim = RoiManager(1)
		if roim.getCount()>0:
			roim.runCommand("Select All")
			roim.runCommand("Delete")
		# load up ROIs
		imp_roi_list = []
	
		# clear out results and rois if already exists
		if os.path.exists(CONFIG["RESULTS_PATH"]): shutil.rmtree(CONFIG["RESULTS_PATH"])
		if os.path.exists(CONFIG["ROIS_PATH"]): shutil.rmtree(CONFIG["ROIS_PATH"])
		if not os.path.exists(os.path.join(CONFIG["BASE_PATH"], CONFIG["LOCAL_PATH"]+"_results")): os.mkdir(os.path.join(CONFIG["BASE_PATH"], CONFIG["LOCAL_PATH"]+"_results"))
		os.mkdir(CONFIG["RESULTS_PATH"])
		os.mkdir(CONFIG["ROIS_PATH"])
	
		#if os.path.exists(os.path.join(BASE_PATH, LOCAL_PATH+"_results")): shutil.rmtree(os.path.join(BASE_PATH, LOCAL_PATH+"_results"))
		#os.mkdir(os.path.join(BASE_PATH, LOCAL_PATH+"_results"))
		#os.mkdir(RESULTS_PATH)
		#os.mkdir(ROIS_PATH)
	
	
		if os.path.exists(os.path.join(CONFIG["ROI_FILE"],"RoiSet.zip")): 
			print "using ROI file built by FIJI"
			CONFIG["ROI_FILE"] = os.path.join(CONFIG["ROI_FILE"], "RoiSet.zip")
			roim.runCommand("Open", CONFIG["ROI_FILE"])
			for roi in roim.getRoisAsArray():
				imp.setRoi(roi)
				imp_roi = imp.duplicate()
				imp_roi_list.append(imp_roi)
	
		elif os.path.exists(os.path.join(CONFIG["ROI_FILE"],"total_ROI")):
			print "using ROI file built by matlab"
			CONFIG["ROI_FILE"] = os.path.join(CONFIG["ROI_FILE"],"total_ROI")
			roi_text = open(CONFIG["ROI_FILE"]).read()
			imp_roi_list = []
			for line in roi_text.split("\n")[:-1]:
				values = map(float, line.split()) # convert strings to floats
				roi = Roi(values[0], values[2], values[1]-values[0], values[3]-values[2]) 
				imp.setRoi(roi)
				imp_roi = imp.duplicate()
				imp_roi_list.append(imp_roi)
		else: 
			print "didn't find any ROI to crop"
			return
		# clean up
		imp.close()
	
		
		# process each individual ROI
		nrois = len(imp_roi_list)
		thread_pool = []
		for r in range(nrois):
			#if r!=105: continue
			#if r != 0: continue
			IJ.log("Processing ROI %d/%d" % (r+1, nrois))
			imp_roi = imp_roi_list[r]
			title = "yeast_ROI%d" % r
	
			if CONFIG["ENABLE_THREADING"]:		
				if len(thread_pool) < CONFIG["NCPUS"]:
					t = threading.Thread(target = analyseRoi, args = (imp_roi, title, CONFIG))
					t.start()
					thread_pool.append(t)
				while len(thread_pool) == CONFIG["NCPUS"]:
					time.sleep(0.1)
					for n in range(len(thread_pool)):
						if not thread_pool[n].isAlive():
							thread_pool.pop(n)
							break
			else:		
				analyseRoi(imp_roi, title, CONFIG)
	
		# dump settings
		settings = file(os.path.join(CONFIG["RESULTS_PATH"], "condition_settings.txt"), 'w') # settings of imaging
		settings.write("timestep(msec)\tpixelSize(nm)\t3Dimages(y/n)\tNbreChannels\n")
		settings.write("%.5f\t%.5f\t%r\t%r\n" % (CONFIG["timeStep"], CONFIG["pixelSize"],int(CONFIG["DO3D"]),CONFIG["NBRE_CHANNELS"]))
		settings.close()
				
	IJ.run("Close All")
	print "done :)"
	IJ.log("done :)")
		

#  generic code to handle jython exceptions and show them in error dialog
import ij, sys, traceback
try: 
    main()
except:
    exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
    info = traceback.format_exception(exceptionType, exceptionValue,exceptionTraceback)
    info = "".join(info)
    ij.IJ.error("HDPALM-jython error", info)
    raise
