#################### Detect custom jython libs ######################
import sys, os; from ij import Prefs
_prefs = Prefs(); _path = _prefs.get("JythonExtra.path", "")
if _path == "" or not os.path.exists(_path):
        _path = IJ.getDirectory("Please select 'jython-libs'")[:-1]
_prefs.set("JythonExtra.path", _path); _prefs.savePreferences()
sys.path.append(_path)
#####################################################################


import ij
import ij.IJ
from edu.uchc.octane import *
import edu.uchc.octane.GaussianResolver as GaussianResolver
import math



def gaussianPSFDetection(x_idx, y_idx, ip_template, config):
	import edu.uchc.octane.Prefs
	edu.uchc.octane.Prefs.sigma_ = config["PSF_rad"]
	edu.uchc.octane.Prefs.kernelSize_ = config["PSF_rad"]/10.
	resolver = GaussianResolver(True) # False for non-zero background
	resolver.setImageData(ip_template)
	refine = not resolver.refine(x_idx, y_idx) # 0 for success
	maxValue_gauss = resolver.getHeightOut()
	
	xc, yc, residue = resolver.getXOut()-.5, resolver.getYOut()-.5, resolver.getResidue()

	return xc, yc, maxValue_gauss, refine, residue


def centerMassDetection(x_idx, y_idx, w, h, ip_mask, ip_template, imp, config):
	#if config["SMOOTHED"]:
	#	print "using smoothed values"
	#else:
	#	print "using raw values"

	## testNeighbours is skipped in order to implement a new confidence test based on std		 
	#testNeighbours(x_idx, y_idx, ip, ip_mask, max_value*LOCAL_THRESH, config)
	## make sure we have at least a 3*3 around peak
	#xstart = x_idx-1
	#ystart = y_idx-1
	#xend = x_idx+2
	#yend = y_idx+2
	#if xstart <0: xstart = 0
	#if ystart <0: ystart = 0
	#if xend >= w: xend = w
	#if yend >= h: yend = h
	#for i in range(xstart, xend):
	#	for j in range(ystart, yend):
	#		ip_mask.setf(i, j, ip_original.get(i,j))
			#ip_mask.setf(i, j, ip.get(i,j))

	
	# create a square of  2n+1pix per side  around peak, round
	# ROI is filled with original or smoothed values and surrounded by 0s (mask)
	xstart = x_idx-config["PSF_rad"]
	ystart = y_idx-config["PSF_rad"]
	xend = x_idx+config["PSF_rad"]+1
	yend = y_idx+config["PSF_rad"]+1
	if xstart <0: xstart = 0
	if ystart <0: ystart = 0
	if xend >= w: xend = w
	if yend >= h: yend = h
	for i in range(xstart, xend):
		for j in range(ystart, yend):
			if ((x_idx-i)**2+(y_idx-j)**2)**0.5 <= config["PSF_rad"]:
				ip_mask.setf(i, j, ip_template.get(i,j))
			#ip_mask.setf(i, j, ip.get(i,j))

	imp.setProcessor(ip_mask)
	#ip = ip_mask

	# find the subpixelic center of the spot
	pixels = ip_mask.getPixels()
	xc = 0.
	yc = 0.

	maxValue = 0
	npixels = 0
	sum_v = 0.

	for i in range(ip_mask.getWidth()):
		for j in range(ip_mask.getHeight()):
			v_mask = ip_mask.getPixelValue(i, j)
			if v_mask != 0:
				v_template = ip_mask.getPixelValue(i, j)
				npixels+=1
				x_idx = i
				y_idx = j
				xc += x_idx*v_template
				yc += y_idx*v_template
				sum_v += v_template
				if maxValue<v_template:
					maxValue = v_template
					
						
	xc /= float(sum_v) or 1
	yc /= float(sum_v) or 1
	#print xc, yc, npixels

	return xc, yc, maxValue, ip_mask, sum_v, npixels

def testNeighbours(x, y, ip, ip_mask, thrsh, config):
	xstart = x-1
	xend = x+2
	ystart = y-1
	yend = y+2
	if xstart < 0: xstart = 0
	if ystart < 0: ystart = 0 
	if xend >= ip.getWidth(): xend = ip.getWidth()-1
	if yend >= ip.getHeight(): yend = ip.getHeight()-1
	
	v_original = ip.get(x,y)
	for i in range(xstart, xend):
		for j in range(ystart, yend):
			if i==x and j==y: continue
			v = ip.getf(i, j)
			if v < v_original*CONFIG["GRAD_TOLERANCE"] and v > thrsh and ip_mask.get(i,j)==0:
				ip_mask.setf(i, j, v) 
				testNeighbours(i, j, ip, ip_mask, thrsh, config)
